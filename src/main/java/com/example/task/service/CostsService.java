package com.example.task.service;

import com.example.task.entity.User;
import com.example.task.modal.Response;
import com.example.task.payload.ReqBoolean;
import com.example.task.payload.ReqCosts;
import com.example.task.payload.ReqFilter;
import com.example.task.payload.ReqSalary;

public interface CostsService {
    Response add(ReqCosts reqCosts, User user);
    Response List(ReqBoolean reqBoolean,User user);
    Response Filter(ReqFilter reqFilter, User user);
    Response Salary(ReqSalary reqSalary,User user);

}
